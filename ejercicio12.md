 ```sql
drop table usuarios;

create table usuarios(
    nombre varchar2(20),
    clave varchar2(10),
    primary key (nombre)
);

describe usuarios;

insert into usuarios (nombre, clave) values ('juanperez','Boca');
insert into usuarios (nombre, clave) values ('raulgarcia','River');

insert into usuarios (nombre, clave) values ('juanperez','payaso');

insert into usuarios (nombre, clave) values (null,'payaso');

update usuarios set nombre='juanperez' where nombre='raulgarcia';

select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='USUARIOS';


```