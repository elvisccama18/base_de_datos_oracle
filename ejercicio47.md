```sql
create table libros(
    codigo number(4),
    titulo varchar2(40) not null,
    autor varchar2(30),
    codigoeditorial number(3) not null,
    precio number(5,2),
    primary key (codigo)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20) not null,
    direccion varchar2(40),
    primary key(codigo)
);

```