```sql
drop table libros;

create table libros(
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar(20),
    precio number(6,2)
);

insert into libros
values('El aleph','Borges','Emece',15.90);

insert into libros
values('Antología poética',null,'Planeta',null);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll',null,19.90);

insert into libros
values('Matematica estas ahi','Paenza','Siglo XXI',15);

insert into libros
values('Martin Fierro','Jose Hernandez',default,40);

insert into libros
values('Aprenda PHP',default,'Nuevo siglo',null);

insert into libros
values('Uno','Richard Bach','Planeta',20);

select count(*)
from libros;

select count(*)
from libros
where editorial='Planeta';

select count(precio)
from libros;

select count(editorial)
from libros;

```