```sql
drop table ingles;
drop table frances;
drop table portugues;

create table ingles(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table frances(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table portugues(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

insert into ingles values('20111222','Ana Acosta','Avellaneda 111');
insert into ingles values('21222333','Betina Bustos','Bulnes 222');
insert into ingles values('22333444','Carlos Caseros','Colon 333');
insert into ingles values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into ingles values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into frances values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('30111222','Fabiana Fuentes','Famatina 666');
insert into frances values('30222333','Gaston Gonzalez','Guemes 777');
insert into portugues values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into portugues values('22333444','Carlos Caseros','Colon 333');
insert into portugues values('30222333','Gaston Gonzalez','Guemes 777');
insert into portugues values('31222333','Hector Huerta','Homero 888');
insert into portugues values('32333444','Ines Ilara','Inglaterra 999');

select nombre, domicilio from ingles
intersect
select nombre, domicilio from frances
intersect
select nombre, domicilio from portugues;

select nombre, domicilio from ingles
intersect
select nombre, domicilio from frances
union
(select nombre, domicilio from ingles
intersect 
select nombre, domicilio from portugues)
union
(select nombre, domicilio from frances
intersect
select nombre, domicilio from portugues);

```