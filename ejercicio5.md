```sql
drop table usuarios;
 
create table usuarios (
    nombre varchar2(30),
    clave varchar2(10)
);

describe usuarios;

insert into usuarios (nombre, clave) values ('Marcelo','Boca');
insert into usuarios (nombre, clave) values ('JuanPerez','Juancito');
insert into usuarios (nombre, clave) values ('Susana','River');
insert into usuarios (nombre, clave) values ('Luis','River');

select * from usuarios where nombre='Marcelo';

select nombre from usuarios where clave='River';

select nombre from usuarios where clave='Santi';

```