```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20)
);

insert into editoriales values(1,'Emece');
insert into editoriales values(2,'Planeta');
insert into editoriales values(3,'Siglo XXI');
insert into libros values(100,'El aleph','Borges',1);
insert into libros values(101,'Martin Fierro','Jose Hernandez',2);
insert into libros values(102,'Aprenda PHP','Mario Molina',5);

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);

alter table editoriales
add constraint PK_editoriales
primary key (codigo);

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);

delete from libros where codigoeditorial=5;

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);

select constraint_name, constraint_type
from user_constraints
where table_name='LIBROS';

select constraint_name, column_name
from user_cons_columns
where table_name='LIBROS';

select constraint_name, constraint_type
from user_constraints
where table_name='EDITORIALES';

insert into libros values(103,'El experto en laberintos','Gaskin',default);

select *from libros;

insert into libros values(104,'El anillo del hechicero','Gaskin',8);

delete from editoriales where codigo=2;

drop table editoriales;

```