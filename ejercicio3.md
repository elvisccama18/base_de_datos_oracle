```sql
drop table libros;

create table libros(
    titulo varchar2(20),
    autor varchar2(15),
    editorial varchar2(10),
    precio number(6,2),
    cantidad number(3,0)
);

describe libros;

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El aleph','Borges','Emece',25.50,100);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Matematica estas ahi','Paenza','Siglo XXI',18.8,200);

select * from libros;

insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Atlantida',10,200);

insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais','Lewis Carroll','Atlantida',10,200);

select * from libros;

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El gato con botas','Anonimo','Atlantida',10,2000);

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El gato con botas','Anonimo','Atlantida',10.123,200);

select * from libros;

```