 ```sql
drop table libros;

create table libros(
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(5,2),
    cantidad number(4)
);

insert into libros
values('Uno','Richard Bach','Planeta',15,100);

insert into libros
values('El aleph','Borges','Emece',24,150);

insert into libros
values('Matematica estas ahi','Paenza','Nuevo siglo',12.5,200);

select titulo, cantidad as stock, precio
from libros;

select titulo, cantidad as "stock disponible", precio
from libros;

select titulo,autor,precio, precio*0.1 as descuento, precio-(precio*0.1) as "precio final"
from libros;

select titulo,precio, precio-(precio*0.1) "precio con descuento"
from libros;
```