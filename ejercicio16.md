 ```sql
drop table libros;

create table libros(
    codigo number(5) not null,
    titulo varchar2(40) not null,
    autor varchar2(30),
    editorial varchar2(15)
);

insert into libros
values (1,'Uno','Richard Bach','Planeta');

insert into libros (codigo, titulo, autor)
values (2,'El aleph','Borges');

select *from libros;

insert into libros (titulo, autor,editorial)
values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta');

select *from libros;

```