```sql
drop table libros;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);

drop table control;
create table control(
    usuario varchar2(30),
    fecha date
);

 -- Creamos un disparador que se dispare cada vez que se ingrese un registro en "libros";
 -- el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha
 -- y la hora en la cual se realizó un "insert" sobre "libros":
create or replace trigger tr_ingresar_libros
    before insert
    on libros
begin
    insert into Control values(user,sysdate);
end tr_ingresar_libros;
 /
 
 -- Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

 -- Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:select *from user_triggers where trigger_name ='TR_INGRESAR_LIBROS';
insert into libros values(100,'Uno','Richard Bach',25);

 -- Verificamos que el trigger se disparó consultando la tabla "control"
 -- para ver si tiene un nuevo registro:select * from control;

 -- Ingresamos dos registros más en "libros":insert into libros values(150,'Matematica estas ahi','Paenza',12);insert into libros values(185,'El aleph','Borges',42);

 -- Verificamos que el trigger se disparó consultando la tabla "control" para ver si tiene
 -- dos nuevos registros:select * from control;

```
