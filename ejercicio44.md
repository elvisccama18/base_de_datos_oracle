```sql
drop table empleados;

create table empleados(
    legajo number(5),
    documento char(8),
    apellido varchar2(25),
    nombre varchar2(25),
    domicilio varchar2(30)
);

alter table empleados
add constraint PK_empleados_legajo
primary key (legajo);

select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';

select index_name, index_type, uniqueness
from user_indexes
where table_name='EMPLEADOS';

create unique index I_empleados_documento
on empleados(documento);

select index_name, index_type, uniqueness
from user_indexes where table_name='EMPLEADOS';

alter table empleados
add constraint UQ_empleados_documento
unique(documento);

select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';

create index I_empleados_apellidonombre
on empleados(apellido,nombre);

select index_name, index_type, uniqueness
from user_indexes
where table_name='EMPLEADOS';

select *from user_objects
where object_type='INDEX';

select index_name,column_name,column_position
from user_ind_columns
where table_name='EMPLEADOS';

insert into empleados values(1,'22333444','Lopez','Juan','Colon 123');
insert into empleados values(2,'23444555','Lopez','Luis','Lugones 1234');
insert into empleados values(3,'24555666','Garcia','Pedro','Avellaneda 987');
insert into empleados values(4,'25666777','Garcia','Ana','Caseros 678');

create unique index I_empleados_apellido
on empleados(apellido);

create unique index I_empleados_nombre
on empleados(nombre);

insert into empleados values(5,'30111222','Perez','Juan','Bulnes 233');

```