 ```sql
drop table libros;

create table libros(
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    edicion date,
    precio number(6,2)
);

insert into libros
values('El aleph','Borges','Emece','10/10/1980',25.33);

insert into libros
values('Java en 10 minutos','Mario Molina','Siglo XXI','05/12/2005',50.65);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Emece','29/11/2000',19.95);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Planeta','27/11/2004',15);

select * from libros
order by titulo;

select titulo,autor,precio
from libros order by 3;

select * from libros
order by editorial desc;

select * from libros
order by titulo,editorial;

select * from libros
order by titulo asc, editorial desc;

select titulo, autor
from libros
order by precio;

select titulo, editorial, precio+(precio*0.1) as "precio con descuento"
from libros
order by 3;

select titulo, editorial, edicion
from libros
order by edicion;

select titulo, extract (year from edicion) as edicion
from libros
order by 2;


```
