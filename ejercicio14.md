 ```sql
drop table visitantes;

create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1),
    domicilio varchar2(30),
    ciudad varchar2(20),
    telefono varchar2(11)
);

insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Ana Acosta',25,'f','Avellaneda 123','Cordoba','4223344');

insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Betina Bustos',32,'fem','Bulnes 234','Cordoba','4515151');

insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Betina Bustos',32,'f','Bulnes 234','Cordoba','4515151');

insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Carlos Caseres',43,'m','Colon 345','Cordoba',03514555666);

select *from visitantes;


```