```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    codigoeditorial number(3),
    primary key (codigo)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20),
    primary key (codigo)
);

insert into editoriales values(1,'Planeta');
insert into editoriales values(2,'Emece');
insert into editoriales values(3,'Paidos');

insert into libros values(1,'Uno',1);
insert into libros values(2,'El aleph',2);
insert into libros values(3,'Aprenda PHP',5);

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo) novalidate;

alter table libros
disable novalidate
constraint FK_LIBROS_CODIGOEDITORIAL;

select constraint_name, constraint_type, status, validated
from user_constraints
where table_name='LIBROS';

insert into libros values(4,'Ilusiones',6);

alter table libros
enable novalidate constraint FK_libros_codigoeditorial;

select constraint_name, constraint_type, status, validated
from user_constraints where table_name='LIBROS';

alter table libros
enable validate constraint FK_libros_codigoeditorial;

truncate table libros;

alter table libros
enable validate constraint FK_libros_codigoeditorial;

select constraint_name, constraint_type, status, validated
from user_constraints where table_name='LIBROS';

```