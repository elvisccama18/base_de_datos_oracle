```sql
drop table afiliados;

create table afiliados(
    numero number(5),
    documento char(8) not null,
    nombre varchar2(30),
    afiliadotitular number(5),
    primary key (documento),
    unique (numero)
);

alter table afiliados
add constraint FK_afiliados_afiliadotitular
foreign key (afiliadotitular)
references afiliados (numero);

insert into afiliados values(1,'22222222','Perez Juan',null);
insert into afiliados values(2,'23333333','Garcia Maria',null);
insert into afiliados values(3,'24444444','Lopez Susana',null);
insert into afiliados values(4,'30000000','Perez Marcela',1);
insert into afiliados values(5,'31111111','Garcia Luis',2);
insert into afiliados values(6,'32222222','Garcia Maria',2);

delete from afiliados where numero=5;

select constraint_name, constraint_type,search_condition
from user_constraints
where table_name='AFILIADOS';

select *from user_cons_columns
where table_name='AFILIADOS';

insert into afiliados values(7,'33333333','Lopez Juana',3);

insert into afiliados values(8,'34555666','Marconi Julio',9);

insert into afiliados values(8,'34555666','Marconi Julio',null);

```