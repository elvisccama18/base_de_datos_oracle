```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(5,2)
);

insert into libros values (1,'Uno','Bach','Planeta',22);
insert into libros values (2,'El quijote','Cervantes','Emece',15);
insert into libros values (2,'Aprenda PHP','Mario Molina','Siglo XXI',-40);

alter table libros
add constraint PK_libros_codigo
primary key (codigo);

alter table libros
add constraint PK_libros_codigo
primary key (codigo) disable novalidate;

select constraint_name, constraint_type, status, validated
from user_constraints
where table_name='LIBROS';

insert into libros values (2,'Momo','Michael Ende','Alfaragua',25);

alter table libros
enable novalidate constraint PK_libros_codigo;

delete libros where titulo='El quijote';
delete libros where titulo='Momo';

alter table libros
enable novalidate constraint PK_libros_codigo;

insert into libros values (2,'Momo','Michael Ende','Alfaragua',25);

select constraint_name, constraint_type, status, validated
from user_constraints
where table_name='LIBROS';

alter table libros
add constraint CK_libros_precio
check(precio>=0);

alter table libros
add constraint CK_libros_precio
check(precio>=0) novalidate;

select constraint_type, status, validated
from user_constraints
where table_name='LIBROS' and
constraint_name='CK_LIBROS_PRECIO';

insert into libros values (3,'Momo','Michael Ende','Alfaragua',-25);

alter table libros
disable constraint CK_libros_precio;

select constraint_type, status, validated
from user_constraints
where table_name='LIBROS' and constraint_name='CK_LIBROS_PRECIO';

insert into libros values (3,'Momo','Michael Ende','Alfaragua',-25);

alter table libros
enable novalidate constraint CK_libros_precio;

select constraint_type, status, validated
from user_constraints
where table_name='LIBROS' and constraint_name='CK_LIBROS_PRECIO';

```