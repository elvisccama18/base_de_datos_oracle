```sql
drop table libros;

create table libros(
    autor varchar2(30),
    editorial varchar2(15)
);

 -- Agregamos el campo "titulo" de tipo varchar2(30) y una restricción "unique":
alter table libros
add titulo varchar2(30)
constraint UQ_libros_autor unique;

 -- Veamos si la estructura cambió:
describe libros;

 -- Agregamos el campo "codigo" de tipo number(4) not null y en la misma
 -- sentencia una restricción "primary key":
alter table libros
add codigo number(4) not null
constraint PK_libros_codigo primary key;

 -- Agregamos el campo "precio" de tipo number(6,2) y una restricción
 -- "check" que no permita valores negativos para dicho campo:
alter table libros
add precio number(6,2)
constraint CK_libros_precio check (precio>=0);

 -- Veamos la estructura de la tabla y las restricciones:
describe libros;

select *from user_constraints where table_name='LIBROS';
```