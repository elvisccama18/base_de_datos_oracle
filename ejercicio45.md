```sql
drop table empleados;

create table empleados(
    legajo number (5),
    documento char(8),
    apellido varchar2(40),
    nombre varchar2(40)
);

create unique index I_empleados_legajo
on empleados(legajo);

alter table empleados
add constraint UQ_empleados_legajo
  unique (legajo);

select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';

alter table empleados
add constraint PK_empleados_documento
primary key(documento);

select constraint_name, constraint_type, index_name
from user_constraints
where table_name='EMPLEADOS';

select index_name,uniqueness
from user_indexes
where table_name='EMPLEADOS';

create index I_empleados_nombre
on empleados(nombre);

create index I_empleados_apellido
on empleados(apellido);

drop index I_empleados_legajo;

select constraint_name, constraint_type, index_name
from user_constraints
where index_name='I_EMPLEADOS_LEGAJO';

drop index I_empleados_nombre;

select *from user_objects
where object_type='INDEX';

drop table empleados;

select *from user_indexes where table_name='EMPLEADOS';

select *from user_objects
where object_type='INDEX';

```