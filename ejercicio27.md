```sql
drop table libros;

create table libros(
    titulo varchar2(40),
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    edicion date,
    precio number(6,2)
);

insert into libros
values('El aleph','Borges','Emece','12/05/2005',15.90);

insert into libros
values('Antología poética','J. L. Borges','Planeta','16/08/2000',null);

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll',null,'25/04/2000',19.90);

insert into libros
values('Matematica estas ahi','Paenza','Siglo XXI','21/12/2006',15);

insert into libros
values('Martin Fierro','Jose Hernandez',default,'22/09/2001',40);

insert into libros
values('Aprenda PHP','Mario Molina','Nuevo siglo','22/05/1999',56.50);

insert into libros
values(null,'Mario Molina','Nuevo siglo',null,45);

select * from libros
where autor like '%Borges%';

select * from libros
where titulo like 'M%';

select * from libros
where titulo not like 'M%';

select * from libros
where autor like '%Carrol_';

select titulo,edicion from libros
where edicion like '__/05%';

select titulo,precio from libros
where precio like '1_,%';

select titulo,precio from libros
where precio not like '%,%';
```