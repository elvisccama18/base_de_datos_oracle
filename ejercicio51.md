```sql
drop table comidas;
drop table postres;

create table comidas(
    codigo number(2),
    nombre varchar2(30),
    precio number(4,2)
);

create table postres(
    codigo number(2),
    nombre varchar2(30),
    precio number(4,2)
);

insert into comidas values(1,'ravioles',5);
insert into comidas values(2,'tallarines',4);
insert into comidas values(3,'milanesa',7);
insert into comidas values(4,'cuarto de pollo',6);
insert into postres values(1,'flan',2.5);
insert into postres values(2,'porcion torta',3.5);

select c.nombre as "plato principal",
p.nombre as "postre"
from comidas c
cross join postres p;

select c.nombre as "plato principal",
p.nombre as "postre",
c.precio+p.precio as "total"
from comidas  c
cross join postres p;

```