```sql
drop table libros;
drop table ofertas;
drop table control;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table ofertas(
    titulo varchar2(40),
    autor varchar2(30),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

create or replace trigger tr_ingresar_ofertas
    before insert
    on ofertas
    for each row
begin
    insert into Control values(user,sysdate);
end tr_ingresar_ofertas;
 /
 
 select *from user_triggers where trigger_name ='TR_INGRESAR_OFERTAS';

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(102,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(105,'El aleph','Borges','Emece',32);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);

insert into ofertas select titulo,autor,precio from libros where precio<30;

select *from control;

```
