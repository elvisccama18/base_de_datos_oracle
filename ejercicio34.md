```sql
select *from all_sequences;

drop sequence sec_codigolibros;

create sequence sec_codigolibros
start with 1
increment by 1
maxvalue 99999
minvalue 1;

select sec_codigolibros.nextval from dual;

select sec_codigolibros.currval from dual;

drop table libros;
create table libros(
    codigo number(5) not null,
    titulo varchar2(40) not null,
    autor varchar2(30),
    editorial varchar2(20),
    primary key(codigo)
);

insert into libros
values(sec_codigolibros.currval,'El aleph', 'Borges','Emece');

insert into libros
values(sec_codigolibros.nextval,'Matematica estas ahi', 'Paenza','Nuevo siglo');

select *from libros;

select object_name,object_type
from all_objects
where object_name like '%LIBROS%';

drop sequence sec_codigolibros;

select object_name,object_type
from all_objects
where object_name like '%LIBROS%';

```