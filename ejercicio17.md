 ```sql

 drop table libros;

create table libros(
    titulo varchar2(40) not null,
    autor varchar2(30) default 'Desconocido' not null,
    editorial varchar2(20),
    precio number(5,2),
    cantidad number(3) default 0
);

insert into libros (titulo,editorial,precio)
values('Java en 10 minutos','Paidos',50.40);

select *from libros;

insert into libros (titulo,editorial)
values('Aprenda PHP','Siglo XXI');

select *from libros;

select column_name,nullable,data_default
from user_tab_columns where TABLE_NAME = 'LIBROS';

insert into libros (titulo,autor,editorial,precio,cantidad)
values ('El gato con botas',default,default,default,default);

select *from libros where titulo='El gato con botas';

insert into libros (titulo,autor,cantidad)
values ('Alicia en el pais de las maravillas','Lewis Carroll',null);

select *from libros where autor='Lewis Carroll';

```