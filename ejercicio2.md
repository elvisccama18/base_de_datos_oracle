```sql
select * from all_tables;

drop table usuarios;

create table usuarios(
    nombre varchar2(30),
    clave varchar2(10)
);

select * from usuarios;

insert into usuarios (nombre, clave) values ('Mariano','payaso');

select *from usuarios;

insert into usuarios (clave, nombre) values ('River','Juan');

insert into usuarios (nombre,clave) values ('Boca','Luis');

select * from usuarios;

insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');

```