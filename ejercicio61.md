```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3),
    primary key (codigo)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20),
    primary key (codigo)
);

insert into editoriales values(1,'Emece');
insert into editoriales values(2,'Planeta');
insert into editoriales values(3,'Siglo XXI');
insert into libros values(1,'El aleph','Borges',1);
insert into libros values(2,'Martin Fierro','Jose Hernandez',2);
insert into libros values(3,'Aprenda PHP','Mario Molina',2);
insert into libros values(4,'El anillo del hechicero','Gaskin',3);

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo)
on delete cascade;

select constraint_name, constraint_type, delete_rule
from user_constraints
where table_name='LIBROS';

delete from editoriales where codigo=1;

select *from libros;

alter table libros
drop constraint FK_LIBROS_CODIGOEDITORIAL;

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo)
on delete set null;

select constraint_name, constraint_type, delete_rule
from user_constraints
where table_name='LIBROS';

delete from editoriales where codigo=2;
select *from libros;

alter table libros
drop constraint FK_LIBROS_CODIGOEDITORIAL;

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo);

select constraint_name, constraint_type, delete_rule
from user_constraints
where table_name='LIBROS';

delete from editoriales where codigo=3;

```