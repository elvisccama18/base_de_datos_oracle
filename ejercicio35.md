```sql

drop sequence sec_codigolibros;

create sequence sec_codigolibros
start with 1
increment by 1
maxvalue 999
minvalue 1
nocycle;

select *from all_sequences where sequence_name='SEC_CODIGOLIBROS';

alter sequence sec_codigolibros
increment by 2
maxvalue 99999;

select *from all_sequences where sequence_name='SEC_CODIGOLIBROS';
```