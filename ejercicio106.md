```sql
drop table libros;
drop table control;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
  usuario varchar2(30),
  fecha date
);

insert into libros values(97,'Uno','Richard Bach','Planeta',25);
insert into libros values(98,'El aleph','Borges','Emece',28);
insert into libros values(99,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(100,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(101,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
insert into libros values(102,'El experto en laberintos','Gaskin','Planeta',20);

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

create or replace trigger tr_borrar_libros
    before delete
    on libros
    for each row
begin
    insert into control values(user,sysdate);
end tr_borrar_libros;
/
 
select *from user_triggers where trigger_name ='TR_BORRAR_LIBROS';

delete from libros where codigo<100;

select *from control;

create or replace trigger tr_borrar_libros
    before delete
    on libros
begin
    insert into control values(user,sysdate);
end tr_borrar_libros;
/
 
select *from user_triggers where trigger_name ='TR_BORRAR_LIBROS';

delete from libros where editorial='Planeta';

select *from control;

```
