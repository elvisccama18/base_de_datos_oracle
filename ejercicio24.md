 ```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    precio number(6,2)
);

insert into libros
values(1,'El aleph','Borges','Emece',15.90);

insert into libros
values(2,'Antología poética','Borges','Planeta',39.50);

insert into libros
values(3,'Java en 10 minutos','Mario Molina','Planeta',50.50);

insert into libros
values(4,'Alicia en el pais de las maravillas','Lewis Carroll','Emece',19.90);

insert into libros
values(5,'Martin Fierro','Jose Hernandez','Emece',25.90);

insert into libros
values(6,'Martin Fierro','Jose Hernandez','Paidos',16.80);

insert into libros
values(7,'Aprenda PHP','Mario Molina','Emece',19.50);

insert into libros
values(8,'Cervantes y el quijote','Borges','Paidos',18.40);

select * from libros
where (autor='Borges') and (precio<=20);

select * from libros
where autor='Borges' or editorial='Planeta';

select * from libros
where not editorial='Planeta';

select * from libros
where (autor='Borges') or (editorial='Paidos' and precio<20);

select * from libros
where (autor='Borges' or editorial='Paidos') and (precio<20);
```