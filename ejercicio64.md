```sql
drop table ingles;
drop table frances;

create table ingles(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table frances(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

insert into ingles values('20111222','Ana Acosta','Avellaneda 111');
insert into ingles values('21222333','Betina Bustos','Bulnes 222');
insert into ingles values('22333444','Carlos Caseros','Colon 333');
insert into ingles values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into ingles values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into frances values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('30111222','Fabiana Fuentes','Famatina 666');
insert into frances values('30222333','Gaston Gonzalez','Guemes 777');

select nombre, domicilio from ingles
union
select nombre, domicilio from frances;

select nombre, domicilio from ingles
union all
select nombre, domicilio from frances;

select nombre, domicilio from ingles
union all
select nombre, domicilio from frances
order by nombre;

select nombre, domicilio, 'ingles' as curso from ingles
union
select nombre, domicilio,'frances' from frances
order by curso;

select nombre as alumno, domicilio from ingles
union
select nombre, domicilio from frances
order by alumno;

```