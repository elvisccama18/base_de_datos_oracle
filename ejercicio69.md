```sql
drop table libros;
drop table editoriales;

create table editoriales(
    codigo number(3),
    nombre varchar2(30),
    primary key(codigo)
);

create table libros(
    titulo varchar2(30),
    editorial number(3),
    autor varchar2(30),
    precio number(6,2),
    constraint FK_libros_editorial
    foreign key(editorial)
    references editoriales(codigo)
);

 -- Eliminamos un campo de la tabla "libros":
alter table libros
drop column precio;

 -- Vemos la estructura de la tabla "libros":
describe libros;

 -- Intentamos eliminar el campo "codigo" de "editoriales":
alter table editoriales
drop column codigo;
 -- Un mensaje indica que la sentencia no fue ejecutada.

 -- Eliminamos el campo "editorial" de "libros":
alter table libros
drop column editorial;

describe libros;
 -- El campo se ha eliminado y junto con él la restricción "foreign key":

select *from user_constraints
where table_name='LIBROS';

 -- Ahora si podemos eliminar el campo "codigo" de "editoriales", pues la
 -- restricción "foreign key" que hacía referencia a ella ya no existe:
alter table editoriales
drop column codigo;

select *from user_constraints
where table_name='EDITORIALES';

-- Agregamos un índice compuesto sobre "titulo" y "autor" de "libros":
create unique index I_libros_titulo
on libros(titulo,autor);

 --Veamos si existe tal índice:
select index_name,column_name,column_position
from user_ind_columns
where table_name='LIBROS';

 --Recuerde que si elimina un campo indizado, su índice también se elimina.
 -- Eliminamos el campo "autor" de "libros":
alter table libros
drop column autor;

 --Veamos si existe el índice compuesto creado anteriormente sobre los
 -- campos "titulo" y "autor" de "libros":
select index_name,column_name,column_position
from user_ind_columns
where table_name='LIBROS';

 -- La tabla ahora solamente consta de un campo, por lo tanto, no puede eliminarse,
 -- pues la tabla no puede quedar vacía de campos:
alter table libros
drop column titulo;
 -- Mensaje de error.

```