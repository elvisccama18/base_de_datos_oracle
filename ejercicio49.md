```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20)
);

alter table libros
add constraint PK_libros
primary key(codigo);

alter table editoriales
add constraint PK_editoriales
primary key(codigo);

insert into editoriales values(1,'Planeta');
insert into editoriales values(2,'Emece');
insert into editoriales values(3,'Siglo XXI');
insert into editoriales values(4,'Norma');
insert into libros values(100,'El aleph','Borges',1);
insert into libros values(101,'Martin Fierro','Jose Hernandez',1);
insert into libros values(102,'Aprenda PHP','Mario Molina',2);
insert into libros values(103,'Java en 10 minutos',null,4);
insert into libros values(104,'El anillo del hechicero','Carol Gaskin',4);

select titulo,nombre as editorial
from libros l
right join editoriales e
on codigoeditorial = e.codigo;

select titulo,nombre as editorial
from libros l
right join editoriales e
on e.codigo=codigoeditorial
where codigoeditorial is not null;

select nombre
from libros l
right join editoriales e
on e.codigo=codigoeditorial
where codigoeditorial is null;}

```