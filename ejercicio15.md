 ```sql
drop table libros;

create table libros(
    codigo number(5) not null,
    titulo varchar2(40) not null,
    autor varchar2(30),
    editorial varchar2(15),
    precio number(6,2),
    cantidad number(4)
);

insert into libros (codigo,titulo,autor,editorial,precio,cantidad) values(1,'El aleph','Borges','Emece',25.60,50000);

insert into libros (codigo,titulo,autor,editorial,precio,cantidad) values(1,'El aleph','Borges','Emece',25.60,100.2);

select *from libros;

insert into libros (codigo,titulo,autor,editorial,precio,cantidad) values(2,'Don quijote','Cervantes','Emece',25.123,100);

select *from libros;

insert into libros (codigo,titulo,autor,editorial,precio,cantidad) values(4,'Uno','Richard Bach','Planeta','50',100);

insert into libros (codigo,titulo,autor,editorial,precio,cantidad) values(5,'Alicia en el pais...','Lewis Carroll','Planeta','50.30',200);

```