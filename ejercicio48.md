```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20)
);
 
alter table editoriales
add constraints UQ_editoriales_codigo
unique (codigo);

alter table libros
add constraints UQ_libros_codigo
unique (codigo);

insert into editoriales values(1,'Planeta');
insert into editoriales values(2,'Emece');
insert into editoriales values(3,'Siglo XXI');
insert into editoriales values(null,'Sudamericana');
insert into editoriales values(null,'Norma');
insert into libros values(100,'El aleph','Borges',1);
insert into libros values(200,'Martin Fierro','Jose Hernandez',1);
insert into libros values(300,'Aprenda PHP','Mario Molina',2);
insert into libros values(400,'Java en 10 minutos',default,4);
insert into libros values(500,'El quijote de la mancha','Cervantes',null);

select titulo,nombre
from editoriales e
left join libros l
on codigoeditorial = e.codigo;

select titulo,nombre
from libros l
left join editoriales e
on codigoeditorial = e.codigo;

select titulo,nombre
from editoriales e
left join libros l
on e.codigo=codigoeditorial
where codigoeditorial is not null;

select nombre
from editoriales e
left join libros l
on e.codigo=codigoeditorial
where codigoeditorial is null;

```