```sql
drop table libros;

create table libros(
    codigo number(5) not null,
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(6,2)
);

alter table libros
add constraint PK_libros_codigo
primary key(codigo);

alter table libros
add constraint CK_libros_precio
check (precio>=0);

alter table libros
add constraint UQ_libros
unique(titulo,autor,editorial);

select *from user_constraints where table_name='LIBROS';

alter table libros
drop constraint PK_LIBROS_CODIGO;

alter table libros
drop constraint CK_LIBROS_PRECIO;

select *from user_constraints where table_name='LIBROS';

```