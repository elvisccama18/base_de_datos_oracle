```sql
drop table alumnos;

create table alumnos(
    legajo char(4),
    apellido varchar2(20),
    nombre varchar2(20),
    documento char(8)
);

alter table alumnos
add constraint PK_alumnos_legajo
primary key(legajo);

alter table alumnos
add constraint UQ_alumnos_documento
unique (documento);

insert into alumnos values('A111','Lopez','Ana','22222222');
insert into alumnos values('A123','Garcia','Maria','23333333');
insert into alumnos values('A230','Perez','Juan','23333333');
insert into alumnos values('A123','Suarez','Silvana','30111222');

select *from user_constraints where table_name='ALUMNOS';

select *from user_cons_columns where table_name='ALUMNOS';

```