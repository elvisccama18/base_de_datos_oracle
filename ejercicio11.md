 ```sql
create table libros(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    autor varchar2(20),
    editorial varchar2(20),
    precio number(6,2)
);

insert into libros values(1,'El aleph','Borges','Emece',15.90);
insert into libros values(2,'Cervantes y el quijote','Borges','Paidos',null);
insert into libros values(3,'Alicia en el pais de las maravillas','Lewis Carroll',null,19.90);
insert into libros values(4,'Martin Fierro','Jose Hernandez','Emece',25.90);
insert into libros (codigo,titulo,autor,precio) values(5,'Antología poética','Borges',25.50);
insert into libros (codigo,titulo,autor) values(6,'Java en 10 minutos','Mario Molina');
insert into libros (codigo,titulo,autor) values(7,'Martin Fierro','Jose Hernandez');
insert into libros (codigo,titulo,autor) values(8,'Aprenda PHP',null);

select *from libros where editorial is null;

select *from libros where editorial is not null;

insert into libros (codigo,titulo,autor,editorial,precio) values(9,'Don quijote','Cervantes','   ',20);

select *from libros;

select *from libros where editorial is null;

select *from libros where editorial='   ';

```