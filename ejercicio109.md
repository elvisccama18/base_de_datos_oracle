```sql
drop table control;
drop table libros;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

create or replace trigger tr_actualizar_precio_libros
    before update of precio
    on libros
    for each row
begin
    insert into control values(user,sysdate);
end tr_actualizar_precio_libros;
/
 
select *from user_triggers where trigger_name ='TR_ACTUALIZAR_PRECIO_LIBROS';

update libros set precio=precio+precio*0.1 where editorial='Nuevo siglo';

select *from control;
 
update libros set autor='Lewis Carroll' where autor='Carroll';

select *from control;

```
