```sql

drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    primary key (codigo)
);

select *from user_constraints where table_name='LIBROS';

drop table libros;
create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15)
);

insert into libros values(1,'El aleph','Borges','Emece');
insert into libros values(1,'Ilusiones','Bach','Planeta');

update libros set codigo=2 where titulo='Ilusiones';

alter table libros
add constraint PK_libros_codigo
primary key(codigo);

select *from user_constraints where table_name='LIBROS';

insert into libros values(1,'El quijote de la mancha','Cervantes','Emece');

update libros set codigo=1 where titulo='Ilusiones';

insert into libros values(null,'El quijote de la mancha','Cervantes','Emece');

describe libros;

alter table libros
add constraint PK_libros_titulo
primary key(titulo);

select *from user_cons_columns where table_name='LIBROS';

```